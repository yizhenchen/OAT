# OAT
Software Anomaly Detection
Getting Started
Prerequisites：
	Java 7
	Python 2
Running the script
1.	Simple Way to run script
We developed a web application that can do data analysis. 
You only need to provide invariant matrix , Class relationship and function call graph, the application will return a set invariant index of invariant matrix which is selected invariants. 
The link is : http://tccl.rit.albany.edu:8080/OAT

2.	Manual to execute all the scripts step by step and get a chance to look inside middle output.
Step1. Clean class graph
	Input: Class relationship
	Ouput:/Data/ [timestamp]_ _class_graph.txt
	Example Command:
Python cleanClassGraph.py 201806182338335classGraphFile.txt
Step2. Clean invariant matrix
	Input: uploaded program invariant matrix
	Ouput:/Data/[timestamp]_All01.txt
 	Example Command:
     python getInvariantsMetrix2.py  201806182338333invariantMetrix.txt
Step3.Alg1 Result
	Input: Step2’s result, cleaned invariant matrix
	Output:/Data/[timestamp]_alg1_result.txt
Example Command:
		python Algorithm1.py 180620181152089_All01.txt /output/dir/
Step4.Get Class-invariant Full
	 Input : uploaded program invariant matrix
	 Output:/Data/[timestamp] _class_invariant.txt
Example Command:
python class_invariant_relation.py 01806182338333invariantMetrix.txt
Step5.Get Class-invariant
       Input : Step4’s result
              Output:/Data/[timestamp] _ All01.txt
	Example Command:
python getInvariantsMetrix.py 180620181154505_class_invariant.txt
Step6. Get Class id
	     Input: step5's reslut and step1's result
    Output:/Data/[timestamp] _class_id.txt
	    Example Command:
python get_class_id.py 180620181156024_All01.txt 180620181151202_class_graph.txt
Step 7. Get Alg2a result
	  Input: step 6 result, step5 result,step1 result, step2 result
Output:/Data/ [timestamp]_ _alg2a_result.txt
Example Command:
python Algorithm2a.py 180620181156437_class_id.txt 180620181156024_All01.txt  180620181151202_class_graph.txt 180620181152089_All01.txt /output/dir
Step 8. Get Function invariant full
Input: invariant metrix
              Output: [timestamp] _fun_invariant.txt             
             Example Command:
python fun_invariant_relation.py 201806182338333invariantMetrix.cvs
Step 9. Clean function call graph
Input: Step 8 result
              Output: /Data/[timestamp]_call_graph.txt             
             Example Command:
python cleanCallGraph.py 180620181158556_fun_invariant.txt 201806182338333callgraphFile.txt
Step 10. Get function-invariant 
Input: Step 8 result
              Output:/Data/[timestamp] _A1101.txt             
             Example Command:
python getInvariantsMetrix.py 180620181158556_fun_invariant.txt
Step 11. Get function id
Input: Step 10 result
Output: /Data/[timestamp] _funID_invariant.txt ,[timestamp]_ _fun_id.txt
              Example Command:
python get_function_id.py 190620181201272_All01.txt
Step 12 Get Alg2b result
Input: Step 11 result
              Output: /Data/[timestamp] _alg2b_result.txt
             Example Command:
Python algAlgorithm2b.py 190620181201594_fun_id.txt 190620181201597_funID_invariant.txt 190620181200538_call_graph.txt Data180620181152089_All01.txt  /output/dir/
3.	Generate invariant matrix
To get anomaly invariant, we have a program that contains faults and we call it the buggy version of program. We fix one bug at a time and call it the fix version of the program. We get the program invariant of fixed version of the program and buggy version of them program which the different is the one fault removal. 
After we have fixed versions and buggy versions, we can generate invariant Matrix 

Step1 : Use invViolates.py to compare fix versions and related buggy versions, get a violate1.txt, violate2.txt
Command:
	Python invViolates.py  fix1.txt bug1.txt  

Step2: Then use "invdiff.py -v” to compare last fixed version and all violate files, and generate a matrix. If matrix file is too big, use ‘-n’ option to output line number instead of Program Points and invariants.
Command:
	Python invdiff.py -v  base.txt[last fixed version of the program]  violate1.txt  violate2.txt …






