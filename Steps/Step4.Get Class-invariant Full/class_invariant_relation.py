import csv
import sys
import time
import argparse
import random
csv.field_size_limit(922337203)
import os
dir_path = os.path.dirname(os.path.realpath(__file__))

# class_invF="Data/time11/class_invariant_raw.txt"
# c_iF="Data/time11/class_invariant.txt"
#
# class_invF="Data/closure4/class_invariant_raw.txt"
# c_iF="Data/closure4/class_invariant.txt"

#input class_invF : class-invariant_raw_data
#      c_iF:output class_invariant
def getClass_Invariant(class_invF,c_iF):
    f = open(c_iF, 'w')
    with open(class_invF, "r") as files:
        reader = csv.DictReader(files)
        writer = csv.DictWriter(f, fieldnames=reader.fieldnames, lineterminator='\n')
        writer.writeheader()
        for row in reader:
            str = row["Program Points"]
            if (":::ENTER" not in str and ":::EXIT" not in str):
                if ("$" in str):
                    row["Program Points"] = str[0:str.find("$")]
                else:
                    if (":::OBJECT" in str):
                        row["Program Points"] = str.replace(":::OBJECT", "")
                    else:
                        row["Program Points"] = str.replace(":::CLASS", "")
            else:
                if ("$" in str):
                    str = str[0:str.find("$")]
                    if ("(" in str):
                        str = str[0:str.find("(")]
                        row["Program Points"] = str[0:str.rfind(".")]
                    else:
                        row["Program Points"] = str[0:str.find("$")]
                else:
                    if ("(" in str):
                        str = str[0:str.find("(")]
                        row["Program Points"] = str[0:str.rfind(".")]

            writer.writerow(row)
    print "Done"


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Get Class_Invariant file")
    parser.add_argument('csv')
    args = parser.parse_args()
    print args.csv
    output_file_name = dir_path+"/Data/"+str(time.strftime("%d%m%Y%I%M%S"))+str(random.randint(1, 10))+"_class_invariant.txt"
    print "Output file :" + output_file_name
    getClass_Invariant(args.csv,output_file_name)

