import json
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
import xml.etree.ElementTree as ET
import argparse
import time, random ,sys


className = {}
dependency_SOURCE = {}
dependency_TARGET = {}
association_SOURCE = {}
association_TARGET = {}
nesting_SOURCE = {}
nesting_TARGET = {}
realization_SOURCE = {}
realization_TARGET = {}
generalization_SOURCE = {}
generalization_TARGET = {}
classRelationShip = []


def cleanClassGraph(classGraphFile):
    tree = ET.parse(classGraphFile)
    root = tree.getroot()
    for child in root:
        if (child.tag == 'class'):
            className[child.attrib['id']] = child.attrib['name']

    for assoc in root.findall('association'):
        sourceid = 0
        targetid = 0
        for end in assoc.findall('end'):

            if (end.attrib["type"] == "SOURCE"):
                if (association_SOURCE.has_key(end.attrib['refId'])):
                    association_SOURCE[end.attrib['refId']] = association_SOURCE[end.attrib['refId']] + 1
                else:
                    association_SOURCE[end.attrib['refId']] = 1

                sourceid = end.attrib['refId']

            if (end.attrib["type"] == "TARGET"):
                if (association_TARGET.has_key(end.attrib['refId'])):
                    association_TARGET[end.attrib['refId']] = association_TARGET[end.attrib['refId']] + 1
                else:
                    association_TARGET[end.attrib['refId']] = 1
                targetid = end.attrib['refId']

            if (sourceid != 0 and targetid != 0):
                if (className.has_key(sourceid) and className.has_key(targetid)):
                    classRelationShip.append(className[sourceid] + "  " + className[targetid])

    for assoc in root.findall('dependency'):
        sourceid = 0
        targetid = 0
        for end in assoc.findall('end'):
            if (end.attrib["type"] == "SOURCE"):
                if (dependency_SOURCE.has_key(end.attrib['refId'])):
                    dependency_SOURCE[end.attrib['refId']] = dependency_SOURCE[end.attrib['refId']] + 1
                else:
                    dependency_SOURCE[end.attrib['refId']] = 1
            sourceid = end.attrib['refId']
            if (end.attrib["type"] == "TARGET"):
                if (dependency_TARGET.has_key(end.attrib['refId'])):
                    dependency_TARGET[end.attrib['refId']] = dependency_TARGET[end.attrib['refId']] + 1
                else:
                    dependency_TARGET[end.attrib['refId']] = 1
            targetid = end.attrib['refId']
            if (sourceid != 0 and targetid != 0):
                if (className.has_key(sourceid) and className.has_key(targetid)):
                    classRelationShip.append(className[sourceid] + " " + className[targetid])

    for assoc in root.findall('nesting'):
        sourceid = 0
        targetid = 0
        for end in assoc.findall('end'):
            if (end.attrib["type"] == "SOURCE"):
                if (nesting_SOURCE.has_key(end.attrib['refId'])):
                    nesting_SOURCE[end.attrib['refId']] = nesting_SOURCE[end.attrib['refId']] + 1
                else:
                    nesting_SOURCE[end.attrib['refId']] = 1
            sourceid = end.attrib['refId']
            if (end.attrib["type"] == "TARGET"):
                if (nesting_TARGET.has_key(end.attrib['refId'])):
                    nesting_TARGET[end.attrib['refId']] = nesting_TARGET[end.attrib['refId']] + 1
                else:
                    nesting_TARGET[end.attrib['refId']] = 1
            targetid = end.attrib['refId']
            if (sourceid != 0 and targetid != 0):
                if (className.has_key(sourceid) and className.has_key(targetid)):
                    classRelationShip.append(className[sourceid] + " " + className[targetid])

    for assoc in root.findall('realization'):
        sourceid = 0
        targetid = 0
        for end in assoc.findall('end'):
            if (end.attrib["type"] == "SOURCE"):
                if (realization_SOURCE.has_key(end.attrib['refId'])):
                    realization_SOURCE[end.attrib['refId']] = realization_SOURCE[end.attrib['refId']] + 1
                else:
                    realization_SOURCE[end.attrib['refId']] = 1
            sourceid = end.attrib['refId']
            if (end.attrib["type"] == "TARGET"):
                if (realization_TARGET.has_key(end.attrib['refId'])):
                    realization_TARGET[end.attrib['refId']] = realization_TARGET[end.attrib['refId']] + 1
                else:
                    realization_TARGET[end.attrib['refId']] = 1

            targetid = end.attrib['refId']
            if (sourceid != 0 and targetid != 0):
                if (className.has_key(sourceid) and className.has_key(targetid)):
                    classRelationShip.append(className[sourceid] + " " + className[targetid])

    for assoc in root.findall('generalization'):
        sourceid = 0
        targetid = 0
        for end in assoc.findall('end'):
            if (end.attrib["type"] == "SOURCE"):
                if (generalization_SOURCE.has_key(end.attrib['refId'])):
                    generalization_SOURCE[end.attrib['refId']] = generalization_SOURCE[end.attrib['refId']] + 1
                else:
                    realization_SOURCE[end.attrib['refId']] = 1
            sourceid = end.attrib['refId']
            if (end.attrib["type"] == "TARGET"):
                if (generalization_TARGET.has_key(end.attrib['refId'])):
                    generalization_TARGET[end.attrib['refId']] = generalization_TARGET[end.attrib['refId']] + 1
                else:
                    generalization_TARGET[end.attrib['refId']] = 1
            targetid = end.attrib['refId']
            if (sourceid != 0 and targetid != 0):
                if (className.has_key(sourceid) and className.has_key(targetid)):
                    classRelationShip.append(className[sourceid] + " " + className[targetid])

classInd = {}
classOutD = {}
classDegree = {}


def calculateDegree(dic_SOURCE, dic_Target):
    for clazz in className.keys():
        classDegree[className[clazz]] = 0
        for d in dic_SOURCE.keys():
            if (clazz == d):
                if (classInd.has_key(className[clazz])):
                    classInd[className[clazz]] = classInd[className[clazz]] + 1
                else:
                    classInd[className[clazz]] = 1

        for d in dic_Target.keys():
            if (clazz == d):
                if (classOutD.has_key(className[clazz])):
                    classOutD[className[clazz]] = classOutD[className[clazz]] + 1


                else:
                    classOutD[className[clazz]] = 1
        if (classInd.has_key(className[clazz])):
            classDegree[className[clazz]] = classDegree[className[clazz]] + classInd[className[clazz]]
        if (classOutD.has_key(className[clazz])):
            classDegree[className[clazz]] = classDegree[className[clazz]] + classOutD[className[clazz]]


calculateDegree(dependency_SOURCE, dependency_TARGET)
calculateDegree(association_SOURCE, association_TARGET)
calculateDegree(nesting_SOURCE, nesting_TARGET)
calculateDegree(realization_SOURCE, realization_TARGET)
calculateDegree(generalization_SOURCE, generalization_TARGET)

def output(output):
    with open(output, "w+")as f:
        for value in classRelationShip:
            f.write(json.dumps(value))
            f.write("\n")
    print "Done"








if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Step2: Run with Alg1")
    parser.add_argument('csv')
    args = parser.parse_args()
    print args.csv
    output_file_name = dir_path+"/Data/" + str(time.strftime("%d%m%Y%I%M%S")) + str(random.randint(1, 10)) + "_class_graph.txt"
    print "Output file :" + output_file_name
    cleanClassGraph(args.csv)
    output(output_file_name)
