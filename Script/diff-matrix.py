#!/usr/bin/env python3
# input two invariant matrix (for validation) A and B
# output a matrix only include invariants that are different between A and B

import os
import sys
import argparse
import csv


def filter(line1, line2, startCol):
    for i in range(startCol, len(line1)):
        if line1[i] == '0':
            continue
        else:
            if line2[i] == '1':
                line1[i] = '0'


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="compare two matrix (csv) output difference")
    parser.add_argument('first', help='matrix A need to compare', type=argparse.FileType('r'))
    parser.add_argument('second', help='base matrix B', type=argparse.FileType('r'))
    parser.add_argument('-n', '--no-names', action='store_true', help="instead of printing program point and invariant names, print line numbers, this option can significantly reduce output file size")
    args = parser.parse_args()

    reader1 = csv.reader(args.first)
    reader2 = csv.reader(args.second)
    writer = csv.writer(sys.stdout)

    # output header
    writer.writerow(next(reader1))
    next(reader2)

    # calculate first data column
    startCol = 1
    if not args.no_names:
        startCol = 3

    for line1 in reader1:
        line2 = next(reader2);
        if line1[0] != line2[0]:
            print("{} != {}".format(line1[0], line2[0]))
            sys.exit(1)
        filter(line1, line2, startCol)
        writer.writerow(line1)



