# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 11:36:14 2015

@author: Adil
"""


import numpy as np
import mutual_info,time
from operator import itemgetter
import time

def C(S):
    return len(S)

def F(S,V,y):
    f=[]
    FS=0.0
    FS_0=0.0
    #print 'y_len',len(y)
    for i in range(len(y)):
        temp=0.0
        for j in S:            
            temp+=V[j][i]
        f.append(temp)
    
    
    T=1.0/sum(y)
    
    for j in range(len(y)):
        if f[j]>=1:
            f[j]=1
        temp=y[j]*f[j]
        #if temp!=0:
            #print y[j],f[j]
        FS_0+=y[j]*f[j]
    
    #print sum(y)
    #print FS_0,S
    #print '---------------------------'
    #for j in range(len(y)):
    #    print f[j],y[j]
    
    FS=T*FS_0
    
    return FS
def readData1(fileName):
    with open(fileName,"r") as fp:    
        data=[]
        d={}
        index=0
        sindex=0
        for line in fp.readlines():
            #print tuple(line.split())
            #line=line.replace("\"","")
            line=[int(i) for i in line.split()]
            if sum(line)>0:
                data.append(line)
                d[sindex]=index
                sindex+=1            
            index+=1    
                
            
    print len(data[0])
    data=np.array(data)
    y=[]
    for j in range(len(data[0])):
        if sum(data[:,j])>=1:
            y.append(1)
        else:
            y.append(0)
        
    print y,sum(y)
    return y,data,d    

def getClassGraph(fileName):
    nodes=[]
    nodes_index={}
    index=0
    edges=[]
    l_class=[]
    with open("Data/ChartPartial_2_index.txt","r") as cp2:
        for e_class in cp2.readlines():
            l_class.append(e_class.split()[1])
            #print l_class
    l_class=list(set(l_class))
    
        
    print 'class_number',len(l_class)
    nodes_id=[2,16,27,66,80,93,109,110,188,212]
    with open(fileName,"r") as fp:
        for edge in fp.readlines():
            edge=edge.split()            
            edges.append((int(edge[0]),int(edge[1])))
            nodes.append(int(edge[0]))
            nodes.append(int(edge[1]))
        nodes=list(set(nodes))
        
        nodes.sort()
        #print nodes,len(nodes)
        for index,node in enumerate(nodes):
            nodes_index[node]=index
        adjMatrix=np.zeros((len(nodes_id),len(nodes_id)))  #adjecent Matrix
        #print len(adjMatrix[0])
        #print nodes_index
        #print nodes,len(nodes)
        for edge in edges:   
            #print nodes_index[edge[0]],nodes_index[edge[1]]
            x=nodes_index[edge[0]]
            y=nodes_index[edge[1]]
            if x in nodes_id and y in nodes_id:
                print x,'-',y
        
        adj_sum=[]
        for i in range(len(adjMatrix)):
            adj_sum.append((i,sum(adjMatrix[i])))
        adj_sum=sorted(adj_sum, key=lambda x: x[1],reverse=True)
        #print adj_sum       
        
        
        print len(edges)
    
   
def S_0(V,y,lambd1):
    S0=[]
    GScore_temp=-1000.0    
    inti_index=0
    t_l=[]
    gs_t=[]
    for i in range(len(V)):
        t_l[:]=[]        
        t_l.append(i)        
        tGS=GS(t_l,V,y,lambd1)
        print t_l ,tGS       
        gs_t.append(tGS)
        if tGS>GScore_temp:                        
            GScore_temp=tGS
            t_l.pop()
            inti_index=i
    print '>>#########',lambd1,'#########'
    S0.append(inti_index)
    #print 's0',tindex
    print S0,GScore_temp
    return S0,GScore_temp    
    
def GS(S,V,y,lambd1):
    #print 'S',S
    #print 'FS',F(S,V,y)
    #print 'C(S)',lambd*C(S)
    G=F(S,V,y)-lambd1*C(S)
    return round(G,4)
    
def cros_mat(S_c,S,cov):
    cros_Mat=np.zeros((len(S_c),len(S)))
    for q,i in enumerate(S_c):
        for k,j in enumerate(S):
            cros_Mat[q][k]=cov[i][j]
    return cros_Mat

    
def main():
    dataFile="Data/ChartPartial_2_data.txt"
    resultData="Data/ChartPartial_Algo2.txt"
    graphData="Data/ChartPartial_Class_graph.txt"

    dataFile = "Data/ChartPartial_2_data.txt"
    resultData = "Data/ChartPartial_Algo2.txt"
    graphData = "Data/ChartPartial_Class_graph.txt"

    dataFile = "Data/batchAgent/batchAgent_data.txt"
    resultData = "Data/batchAgent/batchAgent_Algo2.txt"
    graphData = "Data/batchAgent/Class_graph.txt"

    #dataFile="Data/time3_correct_withindex_data.txt"
    #resultData="Data/time3_correct_result.txt"
    
    nodes=getClassGraph(graphData)
    y,V,d=readData1(dataFile)
    rff=open(resultData,"w")
    
    #lambd1=[i/10.0 for i in range(1,11)]
    print len(V)
    lambd1=[0.1]
    lambd1 = [i / 100.0 for i in range(1, 101, 3)]
    #lambd2=[1]
   
    SL=[]
    for B in range(1,16):        
        for l1 in range(len(lambd1)):
            for l2 in range(1,2):
                GScore_temp=0.0
                gs_t=[]
                tindex=0
                t_l=[]
                S=[]
                S_t=[]
                                  
                for e in range(len(V)):                        
                    if e not in S:
                        S_t.append(e)                        
                        
                        GScore_temp_e=GS(S_t,V,y,lambd1[l1])
                        GScore_temp=GS(S,V,y,lambd1[l1])
                       
                        if GScore_temp_e>=GScore_temp:
                            GS_t=GScore_temp_e
                            tindex=e
                            S.append(e)
                        else:
                            S_t.remove(e)
                    
                    if len(S)>B and S==S_t:
                        break                
                sS=[]                
                for iS in S:
                    sS.append(d[iS])
                SL.append((B,lambd1[l1],GS(S,V,y,lambd1[l1]),round(F(S,V,y),2),len(sorted(S)),sorted(sS)))
        
    print 'B lmbda1 G(S) F(S) C(S) M(S)'
    SL=sorted(SL, key=itemgetter(2),reverse=True)
    print sS,
    rff.write("B lmbda1 lambda2 G(S) F(S) C(S) M(S)]\n")
    
    for ss in SL:
        rff.write(str(ss).replace("(","").replace(")","").replace(","," ")+"\n")        
    print 'done!!!!'#   print ss
    
    rff.close()
    
    
    #print GS([34, 18, 4, 19, 27, 24, 22, 13, 5, 31, 21],V,y,0.6)
if __name__ == '__main__':
    main()
        
            