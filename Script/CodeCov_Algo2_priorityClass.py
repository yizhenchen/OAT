""" Priority Class Invarian Choose"""
import FC_Score_Algo2
import numpy as np
import time,random

#pri_class=["AllScheduledRequestReader","FirstBatchBatchQueue","DepartmentRequestSorter","ScheduledSafeInsertDate","BatchAssigner","RequestToDepartmentMapper","ResilientWorkerSupervisor","StandardBatchAssignmentAlgorithm","BatchResetter","DispatchPriorityMailbox","WorklistEntryDepartmentMatcher","WorkerStatusReportQueueMessageRouter"]
pri_class=["com.commercehub.app.batchagent.collection.actormodel.AllScheduledRequestReader","com.commercehub.app.batchagent.dispatch.FirstBatchBatchQueue","com.commercehub.app.batchagent.collection.actormodel.DepartmentRequestSorter","com.commercehub.app.batchagent.collection.ScheduledSafeInsertDate","com.commercehub.app.batchagent.assignment.BatchAssigner","com.commercehub.app.batchagent.collection.actormodel.RequestToDepartmentMapper","com.commercehub.app.batchagent.dispatch.supervision.ResilientWorkerSupervisor","com.commercehub.app.batchagent.assignment.StandardBatchAssignmentAlgorithm","com.commercehub.app.batchagent.collection.actormodel.BatchResetter","com.commercehub.app.batchagent.model.WorklistEntryDepartmentMatcher","com.commercehub.app.batchagent.dispatch.supervision.WorkerStatusReportQueueMessageRouter","com.commercehub.app.batchagent.testenv.BatchRouterActorE","com.commercehub.app.batchagent.actor.WorklistEntryCacheActor","com.commercehub.app.batchagent.actor.DepartmentBatchAssignmentActor","com.commercehub.app.batchagent.actor.RefreshWorklistEntryCacheActor","com.commercehub.app.batchagent.dispatch.actormodel.WorkerSupervisionActor","com.commercehub.app.batch.actor.BatchUpdateActor","com.commercehub.app.batchagent.dispatch.actormodel.BatchDispatchActor"]
#print len(pri_class)

class_idF= "Data/time12/class_id.txt" #r
class_invF="Data/time12/class_invariant.txt" #r
class_classF="Data/time12/classRelationShip.txt" #r
# TimeNewToOld /TimeOlderToNew /TimeRandom


class_idF= "Data/closure4/class_id.txt" #r
class_invF="Data/closure4/class_invariant.txt" #r
class_classF="Data/closure4/classRelationShip.txt" #r


#closure2new  closure2oldest  closure2r  
paths=["Data/closure2new/closure-","Data/closure2oldest/closure-","Data/closure2r/closure-"]

class_idF= "Data/time12/class_id.txt" #r
class_invF="Data/time12/class_invariant.txt" #r
class_classF="Data/time12/classRelationShip.txt" #r
# TimeNewToOld /TimeOlderToNew /TimeRandom
paths=["Data/TimeNewToOld/time11-","Data/TimeOlderToNew/time12-","Data/TimeRandom/time12-"]
class_idF= "Data/time12/class_id.txt" #r
class_classF="Data/time12/classRelationShip.txt" #r



class_idF= "Data/time12/class_id.txt" #r
class_classF="Data/time12/classRelationShip.txt" #r

class_idF= "Data/closure4/class_id.txt" #r
class_classF="Data/closure4/classRelationShip.txt" #r

class_idF= "Data/time12/class_id.txt" #r
class_classF="Data/time12/classRelationShip.txt" #r

class_idF= "Data/closure4/class_id.txt" #r
class_classF="Data/closure4/classRelationShip.txt" #r

class_idF= "Data/class_id.txt" #r
class_classF="Data/TimeclassRelationShip.txt" #r





class_classF="Data/math/classRelationShip.txt" #r
class_idF= "Data/math/class_id.txt" #r
paths=["Data/math/codecoverage/math-"]


class_classF="Data/CollectionNewData/classRelationShip.txt" #r
class_idF= "Data/collection/Collection_class_id.txt" #r
paths=["Data/collection_codecoverage/collection-"]

class_idF= "Data/time12/class_id.txt" #r
class_classF="Data/time12/classRelationShip.txt" #r
paths=["Data/time_codecoverage/time15-"]

class_idF= "Data/closure4/class_id.txt" #r
class_classF="Data/closure4/classRelationShip.txt" #r
paths=["Data/codecoverage/Closure15-"]
dd=0
rf=open(paths[dd]+"Algo-2a-result"+paths[dd].split("/")[2]+"_Feb12.txt","a+")  
for k in [10,20,30,40,50,60,70,80,90]:#]:#[10,20,30,40,50,60,70,80,90]:
    
    class_invF=paths[dd]+str(k)+"-class-invariant.txt" #r
    
    dataFile = paths[dd]+str(k)+".txt" #r
    for runTime in range(1):
        rf.write(">>>>>>>>>>>>>>>>>>----- "+str(runTime)+" -----<<<<<<<<<<<<<<<<<<<\n")
        print ">>>>>>>>>>>>>>>>>>----- ",runTime," -----<<<<<<<<<<<<<<<<<<<" 
        t0=time.time()
        class_id_dict = {}
        with open(class_idF, "r") as c_idF:
            for line in c_idF.readlines():
                e = line.replace("\n", "").split()
                class_id_dict[e[1].replace("\r", "")] = int(e[0])
        N = len(class_id_dict)
        #print class_id_dict

        class_invar_List = [[] for i in range(N)]
        
        class_numb=len(class_id_dict)
        inv_numb=0
        with open(class_invF, "r") as c_i_F:
            for index, line in enumerate(c_i_F.readlines()):
                #print ">>",line
                inv_numb+=1
                line=line.replace("\n", "").split()[0]
                if class_id_dict.has_key(line.replace("\n", "")):
                    #print "added"
                    class_invar_List[class_id_dict[line.replace("\n", "")]].append(index)
        
        edges = []
        with open(class_classF, "r") as c_c_F:
            for line in c_c_F.readlines():
                e = line.replace("\n", "").split()
                if e[0] != e[1] and class_id_dict.has_key(e[0]) and class_id_dict.has_key(e[1]):
                    edges.append((class_id_dict[e[0]], class_id_dict[e[1]]))
        #print len(edges),edges
        
        adjList = np.zeros((N, N))
        for e in edges:
            adjList[e[0]][e[1]] = 1
            adjList[e[1]][e[0]] = 1
            # pprint.pprint(adjList)
        # pprint.pprint(np.sum(adjList,axis=1))
        
        print "B_I and B_C= ",class_numb,B_C,B_I
        y,V,d=FC_Score_Algo2.readData1(dataFile)
        
        B_I = int(len(d)*0.001)
        B_C = int(class_numb*0.1)
        S_star = []
        C_star = []
        for i, c in enumerate(class_invar_List):
            for s in S_star:
                if s in c:
                    C_star.append(i)
        C_star = list(set(C_star))
        
        #print "C_star: ",C_star
        sorted_degree = []
        for i, deg in enumerate(adjList):
            sorted_degree.append((i, sum(deg)))
        # print sorted_degree
        sorted_degree = sorted(sorted_degree, key=lambda tup: tup[1], reverse=True)
        #print sorted_degree
        for degree in sorted_degree:
            #if len(C_star) <= B_C and degree[0] not in C_star and degree[0] in pri_class_id:
            if len(C_star) <= B_C and degree[0] not in C_star:
                C_star.append(degree[0])
                iMax_deg = 0
                temp_index = 0
                for c in class_invar_List[degree[0]]:
                    
                    if sum(V[c]) > iMax_deg:
                        iMax_deg = sum(V[c])
                        temp_index = c
                        #print temp_index, sum(V[temp_index]), V[temp_index]
                if sum(V[temp_index]) != 0:
                    S_star.append(temp_index)
                else:
                    if len(class_invar_List[degree[0]]) !=0:
                        S_star.append(random.choice(class_invar_List[degree[0]]))
        
        print dataFile,"============================"
        print "Invariant S_star ",len(S_star), S_star
        rf.write(dataFile+"============================\n")
        rf.write(",".join(map(str,S_star))+"\n")
rf.close()    
    #print "Class C_Star ",len(C_star), C_star
    #print time.time()-t0,' sec....'
