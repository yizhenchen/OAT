# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 22:03:18 2016

@author: Adil
"""
import csv
import sys
import time
import argparse
import random

import os
csv.field_size_limit(922337203)
dir_path = os.path.dirname(os.path.realpath(__file__))

# ci_f="Data/closure4/class_invariant.txt" #r
# adj_f="Data/closure4/Class_graph.txt" #w
# classID_F="Data/closure4/class_id.txt" #w
# classRF="Data/closure4/classRelationShip.txt" #r

def get_class_id(ci_f,classID_F,classRF,adj_f):
    class_id=[]
    with open(ci_f,"r") as ciF:
        for line in ciF.readlines():
            class_id.append(line.strip())
    class_id=list(set(class_id))
    adjFile = open(adj_f, "w")
    class_idF = open(classID_F, "w")
    classes_index={}
    for i,classID in enumerate(class_id):
        classes_index[classID]=i
        class_idF.write(str(i)+" "+classID+"\n")
    #print classes_index
    with open(classRF, "r") as fp:
        classes = []
        edges = []
        self_c = 0
        index = 0
        for line in fp.readlines():
            e = line.split()
            edges.append((e[0], e[1]))
            for class_e in line.split():
                if class_e not in classes:
                    self_c += 1
                    index += 1
                classes.append(class_e)
        self_self_edges = 0
        edges_index = []
        for e in edges:
            if e[0] != e[1]:
                if classes_index.get(e[0])!=None and classes_index.get(e[1])!=None:
                    edges_index.append((classes_index[e[0]], classes_index[e[1]]))
                    adjFile.write(str(classes_index[e[0]]) + " " + str(classes_index[e[1]]) + "\n")
                else:
                    continue
            else:
                self_self_edges += 1
        # print edges_index
        # print len(classes), self_c, len(classes_index)
        classes = list(set(classes))
        #print len(classes), len(edges), len(edges_index)
        #print self_self_edges
    adjFile.close()
    class_idF.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Get Class_Invariant file")
    parser.add_argument('ci')
    parser.add_argument('cr')
    args = parser.parse_args()
    print args.ci
    print args.cr
    print args.cr
    classID_F = dir_path+"/Data/"+str(time.strftime("%d%m%Y%I%M%S"))+str(random.randint(1, 10))+"_class_id.txt"
    adj_f = dir_path+"/Data/" + str(time.strftime("%d%m%Y%I%M%S")) + str(random.randint(1, 10)) + "_class_graph.txt"
    print "Output file 1 :" + classID_F
    get_class_id(args.ci, classID_F, args.cr, adj_f)