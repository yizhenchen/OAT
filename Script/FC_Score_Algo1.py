# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 11:36:14 2015

@author: Adil
"""


import numpy as np
import time, random
from operator import itemgetter


def C(S):
    return len(S)

def F(S,V,y):
    f=[]
    FS=0.0
    FS_0=0.0
    #print 'y_len',len(y)
    for i in range(len(y)):
        temp=0.0
        for j in S:            
            temp+=V[j][i]
        f.append(temp)
    
    
    T=1.0/sum(y)
    
    for j in range(len(y)):
        if f[j]>=1:
            f[j]=1
        temp=y[j]*f[j]
        #if temp!=0:
            #print y[j],f[j]
        FS_0+=y[j]*f[j]
    
    #print sum(y)
    #print FS_0,S
    #print '---------------------------'
    #for j in range(len(y)):
    #    print f[j],y[j]
    
    FS=T*FS_0
    
    return FS
def readData1(fileName):
    with open(fileName,"r") as fp:
    #with open("35X5000.txt","r") as fp:
        data=[]
        d={}
        index=0
        #sindex=0
        for line in fp.readlines():
            #print tuple(line.split())
            #line=line.replace("\"","")
            line=[int(i) for i in line.split()]
            data.append(line)
            d[index]=index
            #sindex+=1           
            index+=1    
                
            
    #data=data[34755:43011]        
    #print len(data[0])
    data=np.array(data)
    y=[]
    for j in range(len(data[0])):
        if sum(data[:,j])>=1:
            y.append(1)
        else:
            y.append(0)
        
    #print y,sum(y)
    return y,data,d    

def readData2(fileName):
    with open(fileName,"r") as fp:    
        data=[]
        d={}
        realIndex=0
        sindex=0
        for line in fp.readlines():
            #print tuple(line.split())
            #line=line.replace("\"","")
            line=[int(i) for i in line.strip().split()]
            if sum(line)>0:
                #print sindex,realIndex
                data.append(line)
                d[sindex]=realIndex
                sindex+=1            
            realIndex+=1    
                
            
    #print len(data[0])
    data=np.array(data)
    y=[]
    for j in range(len(data[0])):
        if sum(data[:,j])>=1:
            y.append(1)
        else:
            y.append(0)
        
    #print y,sum(y)
    return y,data,d     
def S_0(V,y,lambd1):
    S0=[]
    GScore_temp=-1000.0    
    inti_index=0
    t_l=[]
    gs_t=[]
    for i in range(len(V)):
        t_l[:]=[]        
        t_l.append(i)        
        tGS=GS(t_l,V,y,lambd1)
        print t_l ,tGS       
        gs_t.append(tGS)
        if tGS>GScore_temp:                        
            GScore_temp=tGS
            t_l.pop()
            inti_index=i
    #print '>>#########',lambd1,'#########'
    S0.append(inti_index)
    #print 's0',tindex
    #print S0,GScore_temp
    return S0,GScore_temp    
    
def GS(S,V,y,lambd1):
    #print 'S',S
    #print 'FS',F(S,V,y)
    #print 'C(S)',lambd*C(S)
    G=F(S,V,y)-lambd1*C(S)
    return round(G,4)
    
def cros_mat(S_c,S,cov):
    cros_Mat=np.zeros((len(S_c),len(S)))
    for q,i in enumerate(S_c):
        for k,j in enumerate(S):
            cros_Mat[q][k]=cov[i][j]
    return cros_Mat
    

    
    
def main():    
    dataFile="Data/closure4/closure_data.txt"
    #resultData="Data/closure4/closure4_Algo1_result.txt"
    paths=["Data/TimeNewToOld/time11-","Data/TimeOlderToNew/time12-","Data/TimeRandom/time13-"]
    paths=["Data/closure2new/closure-","Data/closure2oldest/closure-","Data/closure2r/closure-"]
    paths=["Data/TimeNewToOld/time11-","Data/TimeOlderToNew/time12-","Data/TimeRandom/time13-"]
    
    #collectionNewToOldest collectionOldToNewest  collectionRandom
    paths=["Data/time_codecoverage/time15-"]
    paths=["Data/collectionRandom/collection-"]
    paths=["Data/collection_codecoverage/collection-"]
    paths=["Data/math/codecoverage/math-"]
    
    paths=["Data/TIME/"]
    #mathNewToOldest mathOldToNewest mathRandom
    
    paths=["Data/collectionRandom/collection-"]
    paths=["Data/collection_codecoverage/collection-"]
    paths=["Data/collection-"]
    
    paths=["Data/collection-"]
    paths=["Data/math/math-"]
    paths=["Data/collection-"]
    paths=["Data/math/math-"]
    
    paths=["Data/time12/time12-"]
    
    paths=["Data/closure4/closure-"]
    
    paths=["Data/math/math-"]
    
    paths=["Data/collection-"]
    dd=0
    
    paths=["Data/math/math-"]
    
    rf=open(paths[dd]+"Algo-1-class-100%_"+paths[dd].split("/")[1]+".txt","a+") 
    for k in range(10): #10,20,30,40,50,60,70,80,90
        #dataFile = paths[dd]+"time_data_f.txt"#+str(k)+".txt" #r
        dataFile = paths[dd]+"100.txt"
        print dataFile,"========================="
        rf.write(dataFile+"==========="+str(k)+"==============\n")
        #resultData="Data/closure_Algo1_result"+str(k)+".txt"
        
        y,V,d=readData2(dataFile)
        #rff=open(resultData,"w")
        #GV=[i for i in range(34)]
        lambd1=[i/100.0 for i in range(1,101,4)]
        #print len(V)
        #lambd1=[0.1]
        #lambd2=[1]
        #S=[i for i in range(10,30)]
        #print 'M1  -  M2 = M'
        #for k in range(14):
        #    print M(S,GV,cov,k)
        #K=5
        SL=[]
        t0=time.time()
        #0.02 ,0.04.0.06,0.08,0.10
        #print len(V)
        BB=int(0.5*len(V))
        #print 'Invariant Constraint:',BB
        for B in range(BB,BB+1):
            for l1 in range(len(lambd1)):
                for l2 in range(1,2):
                    GScore_temp=0.0
                    #gs_t=[]
                    tindex=0
                    #t_l=[]
                    S=[]
                    S_back=[]
                    S_t=[]
                    #print '========',lambd1[l1]                  
                    for e in range(len(V)):
                            
                        if e not in S:
                            #print 'u=',u
                            
                            S_t.append(e)
                            #print S_t
                            
                            GScore_temp_e=GS(S_t,V,y,lambd1[l1])
                            GScore_temp=GS(S,V,y,lambd1[l1])
                         
                            if GScore_temp_e>=GScore_temp:
                                GS_t=GScore_temp_e
                                #tindex=e
                                S.append(e)
                            else:
                                S_t.remove(e)
                                if sum(V[e])>0:
                                    S_back.append(d[e])
                                
                        if len(S)>B:
                            break
                  
                    sS=[]
                    #print S,GS_t
                    for iS in S:
                        sS.append(d[iS])
                    SL.append((B,lambd1[l1],GS(S,V,y,lambd1[l1]),round(F(S,V,y),2),len(sorted(S)),sorted(sS)))
        #print len(V)  
        #print 'B lmbda1 G(S) F(S) C(S) M(S)'
        SL=sorted(SL, key=itemgetter(2),reverse=True)       
        #rff.write("B lmbda1 lambda2 G(S) F(S) C(S) M(S)]\n")  
        random.shuffle(S_back)
        inv_num=0
        for ss in SL[:1]:
            rf.write(",".join(map(str,ss[5]))+"\n")
            inv_num=len(ss[5])
            print ss
        rf.write(",".join(map(str,S_back[:500-inv_num]))+"\n")
        print S_back   
        
                
            #rff.write(str(ss).replace("(","").replace(")","").replace(","," ")+"\n")        
        #print 'done!!!!'#   print ss
        #print time.time()-t0,' sec....',len(lambd1)
        #rff.close()
    rf.close()
    
    #[6373, 6601, 23463, 130090, 140786, 244705]
    #print GS([34, 18, 4, 19, 27, 24, 22, 13, 5, 31, 21],V,y,0.6)
if __name__ == '__main__':
    main()
        
            